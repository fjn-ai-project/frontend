#!/bin/bash

APPDIR="build/linux/app_image"
APPIMAGE="$APPDIR/fjn_ai.AppImage"
export ARCH=x86_64

mkdir -p $APPDIR

if [ -f $APPIMAGE ]
then
  rm $APPIMAGE
fi

cp -r build/linux/x64/release/bundle/ $APPDIR
cp assets/icon/icon.png $APPDIR/fjn_ai.png

cat > "$APPDIR/AppRun" <<EOF
#!/bin/bash

cd "\$(dirname "\$0")"
export APPDIR="\$(dirname "\$(readlink -f "\$0")")"
exec ./bundle/fjn_ai
#"$APPDIR"/fjn_ai
EOF

chmod +x "$APPDIR/AppRun"

cat > "$APPDIR/fjn_ai.desktop" <<EOF
[Desktop Entry]
Name=FJN AI
Exec=./fjn_ai
Icon=fjn_ai
Terminal=false
Type=Application
Categories=Utility;
EOF

APPIMAGETOOL=appimagetool-x86_64.AppImage

if ! command -v  $APPIMAGETOOL &> /dev/null
then
  echo "appimagetool could not be found. Downloading"
  wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
  chmod u+x ./appimagetool-*.AppImage
  if awk -F/ '$2 == "docker"' /proc/self/cgroup | read non_empty_input
  then
    echo "Docker detected, extracting appimage"
    ./appimagetool-*.AppImage --appimage-extract
    APPIMAGETOOL=squashfs-root/AppRun
  fi
fi

$APPIMAGETOOL $APPDIR $APPIMAGE

if awk -F/ '$2 == "docker"' /proc/self/cgroup | read non_empty_input & [ -d squashfs-root ]
then
  rm -rf squashfs-root
fi