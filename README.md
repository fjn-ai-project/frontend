# Setup
* Install Flutter, see the [official guide](https://docs.flutter.dev/get-started/install)
* Build for the wanted platform:
  * Web: `flutter build web`
  * Android: `flutter build apk`
  * Windows:
    * `flutter build windows`
    * create a zip-file from `build/windows/runner/Release`
  * Linux:
    * `flutter build linux`
    * downlaod [AppImageKit](https://github.com/AppImage/AppImageKit)
    * run `./linux/create_appimage.sh`
    * output is `build/linux/app_image/fjn_ai.AppImage`
* If you want to force a server run the build command with `--dart-define SERVER=INSERT_SERVER_ADDRESS_HERE`
* e.g: `flutter build apk --dart-define SERVER=http://192.168.2.1:3000`

# Update icons:
* run `flutter pub run flutter_native_splash:create`
* run `flutter pub run flutter_launcher_icons_maker:main`
