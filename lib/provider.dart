import 'package:fjn_ai/utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fjn_ai/api/network/network.dart';
//import 'package:fjn_ai/api/network/stub.dart';

abstract class SettingProvider with ChangeNotifier {
  final String key;

  SettingProvider({required this.key});

  String? _value;

  Future<SharedPreferences> _getInstance() async {
    return await SharedPreferences.getInstance();
  }

  _save(String value) async {
    SharedPreferences prefs = await _getInstance();
    prefs.setString(key, value);
  }

  load() async {
    SharedPreferences prefs = await _getInstance();
    _value = prefs.getString(key);
  }

  String? get value => _value;

  setValue(String value) {
    _value = value;
    _save(value);
    notifyListeners();
  }
}

class DarkThemeProvider extends SettingProvider {
  DarkThemeProvider() : super(key: "theme");

  bool get isDark {
    switch (value) {
      case "light":
        return false;
      default:
        return true;
    }
  }

  set isDark(bool value) {
    setValue(value ? "dark" : "light");
  }
}

class ServerProvider extends SettingProvider {
  Api api;
  ServerProvider()
      : api = Api(),
        super(key: "server");

  @override
  setValue(String value) {
    api.setServer(value).then((res) {
      // only set the server if it is valid
      if (res) {
        super.setValue(value);
      }
    });
  }

  @override
  load() async {
    await super.load();
    if (_value == null && forceServer != null) {
      setValue(forceServer!);
    }
  }

  refresh() async {
    await api.refresh();
    notifyListeners();
  }
}
