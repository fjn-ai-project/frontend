import 'package:fjn_ai/pages/scan/scan.dart';
import 'package:fjn_ai/pages/settings/settings.dart';
import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'draw/page.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  bool settingsOpen = false;

  IndexedStack _buildStack(serverProvider) {
    final home = HomePage(
      serverProvider: serverProvider,
    );
    final scan = ScanPage(
      serverProvider: serverProvider,
    );
    const settings = SettingsPage();
    final children = supportsCamera ? [home, scan, settings] : [home, settings];
    if (children[_currentIndex] == settings) {
      if (!settingsOpen) {
        Provider.of<ServerProvider>(context, listen: false).refresh();
      }
      settingsOpen = true;
    } else {
      settingsOpen = false;
    }
    return IndexedStack(
      index: _currentIndex,
      children: children,
    );
  }

  List<BottomNavigationBarItem> _buildItems() {
    const draw = BottomNavigationBarItem(
      icon: Icon(Icons.image),
      label: "Draw",
    );
    const settings = BottomNavigationBarItem(
      icon: Icon(Icons.settings),
      label: "Settings",
    );
    return supportsCamera
        ? const [
            draw,
            BottomNavigationBarItem(
              icon: Icon(Icons.camera_alt),
              label: "Scan",
            ),
            settings
          ]
        : const [
            draw,
            settings,
          ];
  }

  @override
  void activate() {
    super.activate();
    // hide the soft keyboard if it is still open
    if (MediaQuery.of(context).viewInsets.bottom > 0) {
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }

  @override
  Widget build(BuildContext context) {
    if (mounted) {
      return Scaffold(
        body: Consumer<ServerProvider>(builder: (context, serverProvider, _) {
          return _buildStack(serverProvider);
        }),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: (newIndex) => setState(() {
            _currentIndex = newIndex;
          }),
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey,
          selectedIconTheme: const IconThemeData(
            size: 28,
          ),
          showUnselectedLabels: false,
          elevation: 10.0,
          items: _buildItems(),
        ),
      );
    } else {
      return Container();
    }
  }
}
