import 'dart:math';

import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/utils.dart';
import 'package:fjn_ai/widgets/button.dart';
import 'package:fjn_ai/widgets/scaffold.dart';
import 'package:fjn_ai/widgets/show_error.dart';
import 'package:flutter/cupertino.dart' show CupertinoIcons;
import 'package:flutter/material.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<DarkThemeProvider>(context);
    final titleTextStyle = Theme.of(context).textTheme.headline6;
    final tileTheme = SettingsTileTheme(
      iconColor: Theme.of(context).primaryColor,
      tileColor: Theme.of(context).backgroundColor,
    );

    return MyScaffold(
      appBarTitle: "Settings",
      body: FutureBuilder<PackageInfo>(
          future: PackageInfo.fromPlatform(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            PackageInfo info = snapshot.data!;
            var serverProvider = Provider.of<ServerProvider>(context);
            return Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SettingsList(
                backgroundColor: Theme.of(context).backgroundColor,
                sections: [
                  SettingsSection(
                    title: 'General',
                    titleTextStyle: titleTextStyle,
                    tiles: [
                      SettingsTile.switchTile(
                        title: 'Theme',
                        subtitle: themeChange.isDark ? 'dark' : 'light',
                        leading: Icon(themeChange.isDark
                            ? CupertinoIcons.moon_stars_fill
                            : CupertinoIcons.sun_max_fill),
                        switchValue: themeChange.isDark,
                        onToggle: (value) => themeChange.isDark = value,
                        theme: tileTheme,
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Server',
                    titleTextStyle: titleTextStyle,
                    tiles: [
                      SettingsTile(
                        title: 'Address',
                        leading: const Icon(Icons.cloud),
                        theme: tileTheme,
                        trailing: emptyWidget,
                        subtitle: serverProvider.api.server,
                      ),
                      SettingsTile(
                        title: 'Share',
                        leading: const Icon(Icons.share),
                        theme: tileTheme,
                        onPressed: (context) =>
                            _showQR(context, serverProvider.api.server!),
                      ),
                      if (forceServer == null)
                        SettingsTile(
                          title: 'Change server',
                          leading: const Icon(Icons.autorenew),
                          onPressed: (context) => Navigator.pop(context),
                          theme: tileTheme,
                        ),
                      SettingsTile(
                        title: 'Analyzed characters',
                        leading: const Icon(Icons.list),
                        subtitle: serverProvider.api.serverInfo != null
                            ? serverProvider.api.serverInfo!.analyzedChars
                                .toString()
                            : "server unreachable",
                        theme: tileTheme,
                        trailing: emptyWidget,
                      )
                    ],
                  ),
                  SettingsSection(
                    title: 'About',
                    titleTextStyle: titleTextStyle,
                    tiles: [
                      SettingsTile(
                        title: 'Version',
                        subtitle: info.version,
                        leading: const Icon(CupertinoIcons.info),
                        theme: tileTheme,
                        trailing: emptyWidget,
                      ),
                      SettingsTile(
                        title: 'Homepage',
                        leading: const Icon(Icons.launch),
                        onPressed: (context) =>
                            launch('https://gitlab.com/fjn-ai-project')
                                .then((value) {
                          if (!value) {
                            showError(context, 'could not open homepage');
                          }
                        }),
                        theme: tileTheme,
                      ),
                      SettingsTile(
                        title: 'Licenses',
                        leading: const Icon(CupertinoIcons.doc_plaintext),
                        onPressed: (context) {
                          showLicensePage(
                            context: context,
                            applicationName: info.appName,
                            applicationVersion: info.version,
                          );
                        },
                        theme: tileTheme,
                      ),
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }

  _showQR(BuildContext context, String server) {
    final size = min(MediaQuery.of(context).size.shortestSide * 0.8, 600.0);
    dialog(context,
        content: SizedBox(
          width: size,
          height: size,
          child: QrImage(
            data: server,
            backgroundColor: Colors.white,
          ),
        ),
        actions: [
          MyButton(
            child: const Text("back"),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ]);
  }
}
