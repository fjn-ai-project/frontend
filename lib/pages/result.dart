import 'dart:typed_data';

import 'package:fjn_ai/api/types.dart';
import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/widgets/button.dart';
import 'package:fjn_ai/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void createResult(BuildContext context, Uint8List img) {
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => Result(
        img: img,
      ),
    ),
  );
}

class Result extends StatefulWidget {
  final Uint8List img;

  const Result({Key? key, required this.img}) : super(key: key);

  @override
  State<Result> createState() => _ResultState();
}

class _ResultState extends State<Result> {
  late Image originalImg;

  @override
  initState() {
    super.initState();
    originalImg = Image.memory(widget.img);
  }

  Widget _buildImage(Image img, String subtitle) {
    return Column(
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height / 3,
            maxWidth: MediaQuery.of(context).size.width / 4,
          ),
          child: AspectRatio(
            aspectRatio: 1,
            child: img,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            subtitle,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ],
    );
  }

  Widget _buildResult(AnalysisResult analysisResult) {
    var letters = Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.all(16),
        itemCount: analysisResult.letters.length * 2 - 1,
        itemBuilder: (context, i) {
          if (i.isOdd) {
            return const Divider();
          }
          final index = i ~/ 2;
          return _buildLetter(analysisResult.letters[index]);
        },
      ),
    );
    var original = _buildImage(originalImg, "Original");
    var images = analysisResult.processedImage == null
        ? [
            original,
          ]
        : [original, _buildImage(analysisResult.processedImage!, "Processed")];
    return OrientationBuilder(builder: (context, orientation) {
      bool portrait = orientation == Orientation.portrait;

      var imageBox = portrait
          ? Row(
              children: images,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            )
          : Column(
              children: images,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            );

      var main = [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: imageBox,
        ),
        letters,
      ];

      return portrait ? Column(children: main) : Row(children: main);
    });
  }

  Widget _buildLetter(Letter letter) {
    return ListTile(
      title: Text(letter.letter),
      trailing: Text('${letter.probability}%'),
    );
  }

  Widget _buildProgress() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height / 3,
          ),
          child: AspectRatio(
            aspectRatio: 1,
            child: originalImg,
          ),
        ),
        Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: CircularProgressIndicator(),
            ),
            Text(
              "Waiting for server response",
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var backButton = Padding(
      padding: const EdgeInsets.all(24),
      child: MyButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: const Text("back"),
      ),
    );

    return MyScaffold(
      appBarTitle: "Result",
      body: Center(
        child: FutureBuilder<AnalysisResult>(
          future: Provider.of<ServerProvider>(context, listen: false)
              .api
              .getAndHandleAIResult(context, widget.img),
          builder: (context, analysisResult) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: analysisResult.hasData
                      ? _buildResult(analysisResult.data!)
                      : _buildProgress(),
                ),
                backButton
              ],
            );
          },
        ),
      ),
    );
  }
}
