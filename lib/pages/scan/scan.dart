import 'package:fjn_ai/pages/result.dart';
import 'package:fjn_ai/pages/scan/camera.dart';
import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/widgets/button.dart';
import 'package:fjn_ai/widgets/home.dart';
import 'package:flutter/material.dart';

class ScanPage extends StatefulWidget {
  final ServerProvider serverProvider;

  const ScanPage({Key? key, required this.serverProvider}) : super(key: key);

  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  late Camera _camera;

  @override
  void initState() {
    _camera = Camera();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Home(
      appBarTitle: 'Scan a letter',
      child: _camera,
      actionRequirement: _camera.isInitialized,
      actions: [
        MyButton(
            onPressed: () => _camera.toggleCamera(),
            child: const Text('switch camera')),
        MyButton(onPressed: getChars, child: const Text('analyze'))
      ],
    );
  }

  void getChars() async {
    var img = await _camera.takePicture();
    createResult(context, img!);
  }
}
