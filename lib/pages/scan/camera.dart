import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:fjn_ai/widgets/button.dart';

import 'package:flutter/material.dart' hide Image;

class Camera extends StatefulWidget {
  final _CameraState _cameraState = _CameraState();

  Camera({Key? key}) : super(key: key);

  @override
  _CameraState createState() => _cameraState;

  selectCamera() async => await _cameraState.selectCamera();

  Future<Uint8List?> takePicture() async => await _cameraState.takePicture();

  toggleCamera() async => await _cameraState.toggleCamera();

  bool isInitialized() => _cameraState.isInitialized();
}

class _CameraState extends State<Camera> with WidgetsBindingObserver {
  List<CameraDescription>? _cameras;
  CameraController? _controller;
  int _selected = 0;

  @override
  void initState() {
    super.initState();
    setupCamera();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _controller?.dispose();
    super.dispose();
  }

  bool isInitialized() {
    return _controller != null && _controller!.value.isInitialized;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (!isInitialized()) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      _controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      setupCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_controller == null) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const CircularProgressIndicator(),
            const Padding(
                padding: EdgeInsets.only(top: 16.0),
                child: Text("Connecting to camera")),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text("If this didn't disappear permission is missing..."),
            ),
            MyButton(
              onPressed: setupCamera,
              child: const Text("Try again"),
            ),
          ],
        ),
      );
    } else {
      return CameraPreview(_controller!);
    }
  }

  Future<void> setupCamera() async {
    try {
      _cameras = await availableCameras();
    } on CameraException {
      return;
    }
    var controller = await selectCamera();
    setState(() => _controller = controller);
  }

  selectCamera() async {
    var controller = CameraController(
      _cameras![_selected],
      ResolutionPreset.low,
      enableAudio: false,
    );
    await controller.initialize();
    return controller;
  }

  Future<Uint8List?> takePicture() async {
    try {
      final file = await _controller!.takePicture();
      return file.readAsBytes();
    } catch (e) {
      return null;
    }
  }

  toggleCamera() async {
    int newSelected = (_selected + 1) % _cameras!.length;
    _selected = newSelected;

    var controller = await selectCamera();
    setState(() => _controller = controller);
  }
}
