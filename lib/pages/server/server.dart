import 'dart:math';

import 'package:fjn_ai/pages/server/qr.dart';
import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/widgets/button.dart';
import 'package:fjn_ai/widgets/gradient_appbar.dart';
import 'package:fjn_ai/widgets/show_error.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:pub_semver/pub_semver.dart';

import 'package:fjn_ai/utils.dart';

class ServerScreen extends StatefulWidget {
  const ServerScreen({Key? key}) : super(key: key);

  @override
  State<ServerScreen> createState() => _ServerScreenState();
}

class _ServerScreenState extends State<ServerScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _server = TextEditingController();
  bool connecting = false;

  @override
  void initState() {
    super.initState();
    // check if we saved already a server and if it is valid
    var _serverProvider = Provider.of<ServerProvider>(context, listen: false);
    _serverProvider.load().then((_) {
      final server = _serverProvider.value;
      if (server != null) {
        checkConnection(server);
      }
    });
    _server.text = "";
  }

  @override
  Widget build(BuildContext context) {
    var inputContainer = Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 30),
      child: SizedBox(
        width: min(MediaQuery.of(context).size.width * 0.8, 500.0),
        child: TextFormField(
            controller: _server,
            cursorColor: Theme.of(context).primaryColor,
            autocorrect: false,
            keyboardType: TextInputType.url,
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide:
                    BorderSide(color: Theme.of(context).colorScheme.secondary),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              hintText: 'https://example.com',
              contentPadding: const EdgeInsets.all(20.0),
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter a valid address';
              }
              return null;
            },
            onFieldSubmitted: (_) {
              if (_formKey.currentState!.validate()) {
                checkConnection(_server.text);
              }
            }),
      ),
    );

    return Scaffold(
      appBar: gradientAppBar(context, title: 'Enter the server address'),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: connecting
                ? [
                    inputContainer,
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 16, 0, 8),
                      child: CircularProgressIndicator(),
                    ),
                    Text(
                      "connecting to server",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    supportsQRCamera
                        ? const SizedBox(
                            height: 41,
                          )
                        : emptyWidget,
                  ]
                : [
                    inputContainer,
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: MyButton(
                        child: const Text('connect'),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            checkConnection(_server.text);
                          }
                        },
                      ),
                    ),
                    supportsQRCamera
                        ? MyButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  fullscreenDialog: true,
                                  builder: (_) => QRCodeView(
                                      callback: (server) =>
                                          _server.text = server),
                                ),
                              );
                            },
                            child: const Text('scan QR code instead'),
                          )
                        : const SizedBox(
                            height: 3,
                          ),
                  ],
          ),
        ),
      ),
    );
  }

  checkConnection(String server) async {
    setState(() {
      connecting = true;
    });
    var _serverProvider = Provider.of<ServerProvider>(context, listen: false);
    try {
      final info = await _serverProvider.api.getInfo(server);
      final requiredVersion = info.version.client;
      final packageInfo = await PackageInfo.fromPlatform();
      final packageVersion = Version.parse(packageInfo.version);
      if (!requiredVersion.allows(packageVersion)) {
        await showError(
            context,
            "the server allows only the following versions: $requiredVersion,\n"
            "but the version is $packageVersion.\n"
            "Try to update else the app might not work as expected");
      }
      setState(() {
        connecting = false;
      });
      _serverProvider.setValue(server);
      Navigator.pushNamed(context, '/');
    } catch (e) {
      setState(() {
        connecting = false;
      });
      await showError(context, 'Error while connecting to server:\n$e');
    }
  }
}
