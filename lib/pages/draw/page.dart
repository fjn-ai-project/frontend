import 'package:fjn_ai/pages/result.dart';
import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/widgets/button.dart';
import 'package:fjn_ai/widgets/home.dart';
import 'package:flutter/material.dart';

import 'draw.dart';

class HomePage extends StatefulWidget {
  final ServerProvider serverProvider;

  const HomePage({Key? key, required this.serverProvider}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Draw draw;
  bool hasChanged = false;

  @override
  initState() {
    super.initState();
    draw = Draw(updateCallback: updateCallback);
  }

  updateCallback() {
    setState(() {
      hasChanged = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Home(
      appBarTitle: "Draw a letter",
      actions: [
        MyButton(
          onPressed: clear,
          child: const Padding(
            child: Text('clear'),
            padding: EdgeInsets.all(10),
          ),
        ),
        MyButton(
          onPressed: hasChanged ? getChars : null,
          child: const Padding(
            child: Text('analyze'),
            padding: EdgeInsets.all(10),
          ),
        ),
      ],
      child: draw,
    );
  }

  void getChars() async {
    if (!hasChanged) {
      return;
    }
    final img = await draw.save();
    createResult(context, img!.buffer.asUint8List());
    setState(() {
      hasChanged = false;
    });
  }

  void clear() {
    draw.clear();
    setState(() {
      hasChanged = false;
    });
  }
}
