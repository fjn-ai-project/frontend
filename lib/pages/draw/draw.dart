import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:fjn_ai/utils.dart';

class Draw extends StatefulWidget {
  final Function() updateCallback;
  final _DrawState drawState = _DrawState();

  Draw({Key? key, required this.updateCallback}) : super(key: key);

  @override
  State<Draw> createState() => drawState;

  Future<ByteData?> save() => drawState.save();

  clear() => drawState.clear();
}

class _DrawState extends State<Draw> {
  List<Offset?> points = [];

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 10,
      borderRadius: BorderRadius.circular(20),
      child: Container(
        color: Theme.of(context).backgroundColor,
        child: GestureDetector(
          onPanStart: (details) => update(details.localPosition, context.size),
          onPanUpdate: (details) => update(details.localPosition, context.size),
          onPanEnd: (_) => update(null, null),
          child: CustomPaint(
            painter: DrawingPainter(
              points: points,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
      ),
    );
  }

  update(Offset? offset, Size? size) {
    // don't draw out of box
    if (offset != null && size != null) {
      if (offset.dx < 0 ||
          offset.dx > size.width ||
          offset.dy < 0 ||
          offset.dy > size.height) {
        return;
      }
    }
    setState(() {
      points.add(offset);
    });
    if (offset != null) {
      widget.updateCallback();
    }
  }

  clear() {
    setState(() {
      points.clear();
    });
  }

  Future<ByteData?> save() async {
    double maxSize = 0.0;
    for (var point in points) {
      if (point == null) {
        continue;
      }
      if (point.dx > maxSize) {
        maxSize = point.dx;
      }
      if (point.dy > maxSize) {
        maxSize = point.dy;
      }
    }
    final recorder = PictureRecorder();
    final canvas = Canvas(recorder,
        Rect.fromPoints(const Offset(0, 0), Offset(maxSize, maxSize)));
    canvas.drawColor(Colors.white, BlendMode.src);
    // `black` is the default color for analysis
    final painter = DrawingPainter(points: points, color: Colors.black);
    painter.paint(canvas, Size(maxSize, maxSize));

    final picture = recorder.endRecording();
    final img = await picture.toImage(imgsize, imgsize);
    final pngBytes = await img.toByteData(format: ImageByteFormat.png);
    return pngBytes;
  }
}

class DrawingPainter extends CustomPainter {
  List<Offset?> points;
  Color color;
  final Paint _paint = Paint()
    ..style = PaintingStyle.stroke
    ..strokeWidth = 6.0;

  DrawingPainter({required this.points, required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Offset? prev;
    Path path = Path();

    for (int i = 1; i < points.length - 1; i++) {
      if (points[i] != null) {
        var x = points[i]!.dx;
        var y = points[i]!.dy;
        if (prev != null) {
          path.lineTo(x, y);
        } else {
          path.moveTo(x, y);
        }
      }
      prev = points[i];
    }
    canvas.drawPath(path, _paint..color = color);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
/*
class Painter extends StatefulWidget {
  const Painter({Key? key}) : super(key: key);

  @override
  State<Painter> createState() => _PainterState();
}

class _PainterState extends State<Painter> {
  final line = null;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [buildCurrentPath(context)],
    );
  }

  Widget buildCurrentPath(BuildContext context) {
    return GestureDetector(
      onPanStart: onPanStart,
      onPanUpdate: onPanUpdate,
      onPanEnd: onPanEnd,
      child: RepaintBoundary(
        child: Container(
          color: Theme.of(context).backgroundColor,
          //color: Colors.transparent,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: CustomPaint(painter: MyCustomPainter()),
        ),
      ),
    );
  }

  void onPanStart(DragStartDetails details) {
    setState(() {
      line = DrawLine([point], )
    });
    print('User started drawing');
    final point = details.localPosition;
    print(point);
  }

  void onPanUpdate(DragUpdateDetails details) {
    final point = details.localPosition;
    print(point);
  }

  void onPanEnd(DragEndDetails details) {
    print('User ended drawing');
  }
}

class MyCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..color = Colors.black
      ..strokeWidth = 4.0;
    Path path = Path();

    path.moveTo(0, 250);
    path.lineTo(100, 200);
    path.lineTo(150, 150);
    path.lineTo(200, 50);
    path.lineTo(250, 150);
    path.lineTo(size.width, 250);
    path.lineTo(0, 250);

    path.moveTo(100, 100);
    path.addOval(Rect.fromCircle(center: const Offset(100, 100), radius: 25));
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
*/
