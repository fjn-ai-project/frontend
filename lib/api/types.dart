import 'dart:convert';

import 'package:flutter/material.dart' show BoxFit, Image;
import 'package:pub_semver/pub_semver.dart';

class Info {
  final VersionInfo version;
  final int analyzedChars;

  Info(this.version, this.analyzedChars);

  Info.fromJson(Map<String, dynamic> json)
      : version = VersionInfo.fromJson(json["version"]),
        analyzedChars = json["analyzed"];
}

class VersionInfo {
  final Version server;
  final VersionConstraint client;

  VersionInfo(this.server, this.client);

  VersionInfo.fromJson(Map<String, dynamic> json)
      : server = Version.parse(json["server"]),
        client = VersionConstraint.parse(json["client"]);
}

class Letter implements Comparable<Letter> {
  final String letter;
  final double probability;

  const Letter(this.letter, this.probability);

  @override
  int compareTo(Letter other) {
    return probability.compareTo(other.probability);
  }
}

class AnalysisResult {
  late List<Letter> letters;
  late Image? processedImage;

  AnalysisResult(this.letters) {
    letters.sort((a, b) => b.probability.compareTo(a.probability));
  }

  AnalysisResult.fromJson(Map<String, dynamic> json) {
    List<Letter> a = [];
    for (var letter in json["letters"].entries) {
      var v = letter.value;
      double value = v is String ? double.parse(v) : v;
      a.add(Letter(letter.key, value));
    }
    // TODO
    /*List<Letter> a = json["letters"]
        .entries
        .map((val) => Letter(val.key, val.value))
        .toList();*/
    // sort by reverse probability
    a.sort((a, b) => b.probability.compareTo(a.probability));
    letters = a;
    String? img = json["processedImage"];
    processedImage = img == null ? null : Image.memory(base64Decode(img), fit: BoxFit.fill,);
  }
}
