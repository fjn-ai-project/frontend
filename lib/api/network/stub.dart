/// replacement for `network.dart` to work without a server
import 'dart:typed_data';

import 'package:fjn_ai/api/network/network.dart' as network;
import 'package:fjn_ai/api/types.dart';
import 'package:pub_semver/pub_semver.dart';

// TODO remove this file

class Api extends network.Api {
  @override
  Future<AnalysisResult> getAIResult(Uint8List img) async {
    if (server == null) {
      throw network.NoServerSelectedException();
    }
    return AnalysisResult([
      const Letter("a", 0.52),
      const Letter("b", 0.22),
      const Letter("c", 0.15),
      const Letter("d", 0.11),
    ]);
  }

  @override
  Future<Info> getInfo(String server) async {
    if (server == this.server && serverInfo != null) {
      return serverInfo!;
    }
    return Info(VersionInfo(Version(1, 0, 0), VersionConstraint.any), 2);
  }
}
