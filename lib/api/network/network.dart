import 'dart:convert';
import 'dart:typed_data';

import 'package:fjn_ai/api/types.dart';
import 'package:fjn_ai/utils.dart';
import 'package:fjn_ai/widgets/show_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class NoServerSelectedException implements Exception {}

class InvalidServerResponseException implements Exception {}

class Api {
  String? _server;
  Info? _serverInfo;

  Info? get serverInfo => _serverInfo;

  String? get server => _server;

  Uri _createUri(String server, String? path) {
    if (!server.endsWith('/')) {
      server = server + "/";
    }
    if (path != null) {
      server += path;
    }
    return Uri.parse(server);
  }

  Future<bool> setServer(String server) async {
    try {
      final info = await getInfo(server);
      _server = server;
      _serverInfo = info;
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<AnalysisResult> getAndHandleAIResult(
      BuildContext context, Uint8List img) async {
    try {
      return await getAIResult(img);
    } on NoServerSelectedException {
      if (forceServer != null) {
        showError(context, 'server unreachable');
      } else {
        showError(context, 'no server selected');
        Navigator.of(context).popAndPushNamed('/server');
      }
      throw NoServerSelectedException();
    }
  }

  Future<AnalysisResult> getAIResult(Uint8List img) async {
    if (_server == null) {
      throw NoServerSelectedException();
    }
    var request =
        http.MultipartRequest("POST", _createUri(_server!, "analyze"));
    final Map<String, String> headers = {"Content-Type": "multipart/form-data"};
    request.files.add(
      http.MultipartFile.fromBytes(
        "image",
        img,
        filename: "image.png",
        contentType: MediaType("image", "png"),
      ),
    );
    request.headers.addAll(headers);
    final response = await request.send();
    final body = await response.stream.bytesToString();
    final json = jsonDecode(body);
    return AnalysisResult.fromJson(json);
  }

  Future<Info> getInfo(String server) async {
    final response = await http.get(_createUri(server, "info"));
    if (response.isRedirect || response.statusCode != 200) {
      throw InvalidServerResponseException();
    }
    final json = jsonDecode(response.body);
    _serverInfo = Info.fromJson(json);
    return _serverInfo!;
  }

  Future<void> refresh() async {
    if (_server != null) {
      await getInfo(_server!);
    }
  }

  Future<bool> isValidServer(String server) async {
    try {
      await getInfo(server);
      return true;
    } catch (e) {
      return false;
    }
  }
}
