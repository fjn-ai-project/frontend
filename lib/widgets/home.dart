import 'package:fjn_ai/widgets/scaffold.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  final Widget child;
  final String? appBarTitle;
  final List<Widget> actions;
  final Function? actionRequirement;

  const Home(
      {Key? key,
      required this.child,
      this.appBarTitle,
      required this.actions,
      this.actionRequirement})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBarTitle: appBarTitle,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ConstrainedBox(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height - 220),
              child: AspectRatio(
                aspectRatio: 1,
                child: child,
              ),
            ),
            actionRequirement == null || actionRequirement!()
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: actions,
                  )
                : const Spacer(),
          ],
        ),
      ),
    );
  }
}
