import "package:flutter/material.dart";

import 'package:fjn_ai/styles.dart';

PreferredSize gradientAppBar(BuildContext context,
    {String? title, double barHeight = kToolbarHeight}) {
  return PreferredSize(
    child: GradientAppBar(
      title: title,
      barHeight: barHeight,
    ),
    preferredSize: Size(MediaQuery.of(context).size.width, barHeight),
  );
}

class GradientAppBar extends StatelessWidget {
  final String? title;
  final double barHeight;

  const GradientAppBar({Key? key, this.title, required this.barHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 30.0,
          top: 8.0,
          bottom: 8.0,
          right: 30.0,
        ),
        child: Center(
          child: Text(
            title ?? '',
            style: const TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.w500,
                color: Colors.white),
          ),
        ),
      ),
      decoration: const BoxDecoration(
        gradient: Styles.primaryGradient,
      ),
    );
  }
}
