import 'package:fjn_ai/styles.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final BorderRadiusGeometry? borderRadius;
  final double? width;
  final double height;
  final Gradient gradient;
  final VoidCallback? onPressed;
  final Widget child;

  const MyButton(
      {Key? key,
      required this.child,
      this.onPressed,
      this.borderRadius,
      this.width,
      this.height = 44.0,
      this.gradient = Styles.primaryGradient})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final borderRadius = this.borderRadius ?? BorderRadius.circular(20.0);
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(gradient: gradient, borderRadius: borderRadius),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
            primary: Colors.transparent,
            shadowColor: Colors.transparent,
            shape: RoundedRectangleBorder(borderRadius: borderRadius)),
        child: child,
      ),
    );
  }
}

class MyIconButton extends StatelessWidget {
  final IconData icon;
  final VoidCallback onPressed;
  final double? iconSize;

  const MyIconButton(
      {Key? key, required this.icon, required this.onPressed, this.iconSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).primaryColor;
    final splashColor = Theme.of(context).splashColor;
    return InkResponse(
      hoverColor: splashColor,
      splashColor: splashColor,
      child: IconButton(
        onPressed: onPressed,
        iconSize: iconSize ?? 72,
        hoverColor: splashColor,
        splashColor: splashColor,
        icon: Icon(
          icon,
          color: color,
        ),
      ),
    );
  }
}
