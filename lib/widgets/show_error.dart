import 'package:flutter/material.dart';

showError(BuildContext context, String text) async {
  await dialog(
    context,
    title: Text(
      'Error',
      style: TextStyle(
        color: Theme.of(context).errorColor,
      ),
    ),
    content: Text(
      text,
      style: Theme.of(context).textTheme.subtitle1,
    ),
  );
}

showWarning(BuildContext context, String text) async {
  await dialog(
    context,
    title: Text(
      'Warning',
      style: TextStyle(
        color: Theme.of(context).errorColor,
      ),
    ),
    content: Text(
      text,
      style: Theme.of(context).textTheme.subtitle1,
    ),
  );
}

dialog(BuildContext context,
    {Widget? title, required Widget content, List<Widget>? actions}) async {
  await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        backgroundColor: Theme.of(context).backgroundColor,
        title: title,
        content: content,
        actions: actions ??
            [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Ok',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ],
      );
    },
  );
}
