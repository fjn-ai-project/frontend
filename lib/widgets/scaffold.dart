import 'package:fjn_ai/widgets/gradient_appbar.dart';
import 'package:flutter/material.dart';

class MyScaffold extends StatelessWidget {
  final Widget body;
  final Widget? bottomNavigationBar;
  final String? appBarTitle;

  const MyScaffold(
      {Key? key,
      required this.body,
      this.bottomNavigationBar,
      this.appBarTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: gradientAppBar(context, title: appBarTitle),
      body: body,
      bottomNavigationBar: bottomNavigationBar,
    );
  }
}
