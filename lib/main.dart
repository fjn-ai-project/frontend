import 'package:fjn_ai/pages/main.dart';
import 'package:fjn_ai/pages/server/server.dart';
import 'package:fjn_ai/provider.dart';
import 'package:fjn_ai/styles.dart';
import 'package:fjn_ai/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (isMobile) {
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  DarkThemeProvider themeChangeProvider = DarkThemeProvider();
  ServerProvider serverChangeProvider = ServerProvider();

  static const initialRoute = forceServer != null ? '/' : '/server';
  static final routes = forceServer != null
      ? {'/': (context) => const MainScreen()}
      : {
          '/': (context) => const MainScreen(),
          '/server': (context) => const ServerScreen(),
        };

  @override
  void initState() {
    super.initState();
    if (forceServer != null) {
      serverChangeProvider.setValue(forceServer!);
    }
    themeChangeProvider.load();
    serverChangeProvider.load();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => themeChangeProvider,
        ),
        ChangeNotifierProvider(
          create: (_) => serverChangeProvider,
        ),
      ],
      child: Consumer<DarkThemeProvider>(
        builder: (BuildContext context, value, Widget? child) {
          return MaterialApp(
            title: 'FJN AI',
            theme: Styles.lightTheme,
            darkTheme: Styles.darkTheme,
            themeMode:
                themeChangeProvider.isDark ? ThemeMode.dark : ThemeMode.light,
            initialRoute: initialRoute,
            routes: routes,
          );
        },
      ),
    );
  }
}
