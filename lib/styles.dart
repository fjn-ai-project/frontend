import 'package:flutter/material.dart';

class Styles {
  static ThemeData themeData(bool isDark, BuildContext context) {
    return isDark ? darkTheme : lightTheme;
  }

  //static const primaryColor = Color.fromARGB(255, 88, 76, 233);
  static const primaryColor = Color.fromARGB(255, 97, 84, 255);

  static final sharedTheme = ThemeData(
    //primaryColor: const Color.fromARGB(255, 73, 63, 194),
    primaryColor: primaryColor,
    // TODO: customize color:
    // https://material.io/design/color/the-color-system.html#color-usage-and-palettes
    primarySwatch: Colors.deepPurple,
    splashColor: Colors.deepPurple,
    buttonTheme: const ButtonThemeData(
      buttonColor: primaryColor,
    ),
    appBarTheme: const AppBarTheme(
      backgroundColor: primaryColor,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(primaryColor),
      ),
    ),
  );

  // TODO improve the themes
  static final lightTheme = sharedTheme.copyWith(
    brightness: Brightness.light,
    backgroundColor: Colors.white,
    canvasColor: Colors.white,
    cardColor: Colors.grey[50],
    scaffoldBackgroundColor: Colors.white,
    colorScheme: sharedTheme.colorScheme.copyWith(secondary: Colors.black),
    textTheme: Typography().black,
  );

  static final darkTheme = sharedTheme.copyWith(
    brightness: Brightness.dark,
    backgroundColor: const Color.fromARGB(255, 34, 39, 46),
    canvasColor: const Color.fromARGB(255, 34, 39, 46),
    cardColor: const Color.fromARGB(255, 51, 59, 69),
    scaffoldBackgroundColor: const Color.fromARGB(255, 34, 39, 46),
    colorScheme: sharedTheme.colorScheme.copyWith(secondary: Colors.grey[200]),
    textTheme: Typography().white,
    hintColor: Colors.grey,
  );

  static const primaryGradient = LinearGradient(colors: [
    primaryColor,
    Colors.deepPurple,
  ]);
}

// TODO: use this
TextTheme getTextTheme(BuildContext context) {
  final style = Theme.of(context).textTheme;
  final shortest = MediaQuery.of(context).size.shortestSide;
  final factor = shortest < 400 ? 1.0 : 1.3;
  return style.apply(fontSizeFactor: factor);
}
