import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:universal_io/io.dart';

final supportsQRCamera = kIsWeb || Platform.isAndroid || Platform.isIOS;
final supportsCamera = supportsQRCamera || Platform.isWindows;
final isMobile = Platform.isAndroid || Platform.isIOS;

const emptyWidget = SizedBox.shrink();

/// size in pixels for the image send to the server
const int imgsize = 728;

/// rate for the refreshs of the result of the AI
const Duration refreshRate = Duration(seconds: 3);

/// force the server to a specific value
/// This removes the ability to switch the server
const String? forceServer =
    bool.hasEnvironment("SERVER") ? String.fromEnvironment("SERVER") : null;
